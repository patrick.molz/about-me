import math


class Node:

    def __init__(self, l=0, r=0, v=0):
        self.l = l
        self.r = r
        self.value = v


def build(tree, p: int, l: int, r: int):
    tree[p].l = l
    tree[p].r = r
    if l == r:
        return
    middle = (l+r) // 2
    build(tree, 2*p+1, l, middle)
    build(tree, 2*p+2, middle+1, r)


def maxInInterval(tree, p: int, l: int, r: int) -> int:
    if l > tree[p].r or r < tree[p].l:
        return 0
    if l <= tree[p].l and tree[p].r <= r:
        return tree[p].value
    return max(maxInInterval(tree, 2*p+1, l, r), maxInInterval(tree, 2*p+2, l, r))


def update(tree, p: int, l: int, r: int, v: int) -> int:
    if r < tree[p].l or l > tree[p].r:
        return
    if tree[p].l != tree[p].r:
        update(tree, 2*p+1, l, r, v)
        update(tree, 2*p+2, l, r, v)
        tree[p].value = max(tree[2*p+1].value, tree[2*p+2].value)
    else:
        tree[p].value = v


if __name__ == "__main__":
    file = open("tests.txt", "r")

    number_of_tests = int(file.readline().split()[0])
    for t in range(0, number_of_tests):
        # Skip empty line
        file.readline()
        n, k = map(int, file.readline().split()[0:2])

        height = math.ceil(math.log2(n))
        size = 2 ** (height+1)
        tree = []
        for i in range(0, size+1):
            tree.append(Node())
        build(tree, 0, 0, n-1)
        print("Case:", t+1)

        for i in range(0, k):
            w, h, p = map(int, file.readline().split()[0:3])
            update(tree, 0, p, p+w-1, maxInInterval(tree, 0, p, p+w-1) + h)

            print("{} ".format(tree[0].value), end="")
    file.close()
