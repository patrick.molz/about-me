# Description

In this challenge multiple blocks of varying sizes will be dropped onto a grid at a given position. After every new block the highest point in the interval should be printed. Structual integrity should be ignored.

In the first example the solution sequence is `1 3 3`. After adding the first block, the maximal height is 1. Adding the second block increases the height then to 3, at which it stays.

In the second example the solution sequence is `5 5 7 7`. Although gravity might pull block 3 down, this is not an issue here.

![](examples.svg)

## Input

First a Integer t will be provided indicating the number of test cases. Each test case is seperated by a newline.
It is followed with one line containing two integers n,k. n is the length of the grid and k is the number of blocks being dropped.
k lines follow containing 3 integers w,h,p. w is the width and h the height of the block. p indicates the position in the grid starting with p=0 at the left most position.

## Output

For each inserted block print the highest position inside the whole range.

## Explanation

To solve this problem using a segment tree is useful. A segment tree is a binary tree, where each node saves a value in a specified interval. In this case the root node saves the max value in the whole interval and each child node halves the interval of the parent. A leaf node then represents the height of a single element in the corresponding grid.
The tree itself is saved in an array, where the root is at index `0` and the children of each node are at `parentIndex * 2 + 1` and `parentIndex * 2 + 2`.

This data structure is useful here, because before adding a new block the max value in a given interval can be queried. With this value the tree can be updated to its new height. Due to the tree structure this means `update` and `max` operations are in `O(log n)`.
This implementation could be further improved by using lazy propagation of updates.

![](segmentTree.svg)
