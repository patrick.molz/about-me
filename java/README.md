# Description

The goal is to find **all** paths between two nodes in a directed graph with the shortest distance.
In **example 1** there are two paths to go from Node 1 to Node 3.
Path 1 -> 3 has distance 3.
Path 1 -> 2 -> 3 has distance 3 aswell.

In **example 2** there is only one shortest path, going directly from 1 -> 4 with distance 2. All other paths have a greater distance.

![](resources/example.svg)

## Input

```
m n
a b c
...
a b c
```

`m` indicates the number of nodes in the graph
`n` indicates the number of edges in the graph. `n` lines follow
`a b c` indicates there is an edge from Node with index `a` to Node with index `b` with distance `c`. Indexing starts with 1.

## Output

Should print all paths from Node 1 to Node m that have minimal distance.

## Explanation

Each Node saves a List of Edges, predecessors, distance from the starting point, its index for identification and a boolean to check if the node has been visited.

The algorithm uses an adjusted version of `Dijkstra's algorithm`. I keeps a List of predecessor nodes each time a Node can be reached with equal distance.
Once it is completed the paths can be backtracked using the saved predecessors, starting from the destination to build the paths.
