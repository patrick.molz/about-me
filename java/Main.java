import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Comparator;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

class Node {
    LinkedList<Edge> edges = new LinkedList<Edge>();
    LinkedList<Node> predecessors = new LinkedList<Node>();
    int distance = Integer.MAX_VALUE;
    boolean visited = false;
    int index;

    Node(int i) {
        this.index = i;
    }

    static Comparator<Node> compare = new Comparator<Node>() {
        public int compare(Node a, Node b) {
            return a.distance - b.distance;
        }
    };
}

class Edge {
    Node from;
    Node to;
    int distance;

    Edge(Node from, Node to, int distance) {
        this.from = from;
        this.to = to;
        this.distance = distance;
    }
}

class Graph {
    Node[] nodes;

    Graph(int n) {
        nodes = new Node[n];
        for (int i = 0; i < n; i++) {
            nodes[i] = new Node(i + 1);
        }
    }

    public void addEdge(Node from, Node to, int distance) {
        Edge e = new Edge(from, to, distance);
        from.edges.add(e);
    }

}

public class Main {

    public static void main(String[] args) {
        String[] testfiles = new String[] { "test.txt", "test2.txt", "test3.txt", "test4.txt", "test5.txt" };

        for (int i = 0; i < testfiles.length; i++) {
            Graph graph = getTestData(testfiles[i]);
            System.out.println("Case: " + (i + 1));
            solveTestCase(graph);
        }
    }

    public static Graph getTestData(String filepath) {
        BufferedReader reader;
        Graph graph = null;
        try {
            reader = new BufferedReader(new FileReader(filepath));
            String[] line = reader.readLine().split(" ");

            // Read parameters in first line
            int m = Integer.parseInt(line[0]);
            int n = Integer.parseInt(line[1]);

            graph = new Graph(m);
            // Read M lines, using 3 integers a,b,c = from,to,length
            for (int i = 0; i < n; i++) {
                line = reader.readLine().split(" ");
                int a = Integer.parseInt(line[0]) - 1;
                int b = Integer.parseInt(line[1]) - 1;
                int c = Integer.parseInt(line[2]);
                graph.addEdge(graph.nodes[a], graph.nodes[b], c);
            }
            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return graph;
    }

    private static void solveTestCase(Graph graph) {
        PriorityQueue<Node> pq = new PriorityQueue<Node>(Node.compare);
        graph.nodes[0].distance = 0;
        pq.add(graph.nodes[0]);

        while (!pq.isEmpty()) {
            Node current = pq.poll();
            if (current.visited) {
                continue;
            }
            current.visited = true;
            for (Edge e : current.edges) {
                Node next = e.to;
                if (current.distance + e.distance == next.distance) {
                    next.predecessors.add(current);
                } else if (current.distance + e.distance < next.distance) {
                    next.distance = current.distance + e.distance;
                    pq.remove(next);
                    pq.add(next);
                    next.predecessors.clear();
                    next.predecessors.add(current);
                }
            }
        }
        printShortestPaths(graph);
    }

    private static void printShortestPaths(Graph graph) {
        Node destination = graph.nodes[graph.nodes.length - 1];

        LinkedList<LinkedList<Node>> paths = getPaths(graph, destination);
        System.out.println(String.format("Number of paths: %d with length %d", paths.size(), destination.distance));
        for (LinkedList<Node> path : getPaths(graph, destination)) {
            for (Node n : path) {
                System.out.print(n.index + " ");
            }
            System.out.print("\n");
        }
    }

    private static LinkedList<LinkedList<Node>> getPaths(Graph graph, Node n) {
        if (n.equals(graph.nodes[0])) {
            LinkedList<LinkedList<Node>> paths = new LinkedList<LinkedList<Node>>();
            LinkedList<Node> path = new LinkedList<Node>();
            path.add(n);
            paths.add(path);
            return paths;
        }
        LinkedList<LinkedList<Node>> paths = new LinkedList<LinkedList<Node>>();
        for (Node pre : n.predecessors) {
            paths.addAll(getPaths(graph, pre));
        }
        for (LinkedList<Node> path : paths) {
            path.add(n);
        }
        return paths;
    }
}
