# About Me

This repository shows some code that I wrote in my favourite languages. Every sub directory contains another README.md with further info.

# More

[this-is-patrick.net](https://this-is-patrick.net)
[this-is-patrick.net on gitlab](https://gitlab.com/patrick.molz/this-is-patrick)
