# Description

Making change is a common problem that is easily explained. Given an amount of money and a set of coins with different value, calculate the minimal number of coins required to reach the desired amount.

# Input

Each test case begins with 2 numbers `n` and `c`. `n` being the number of distinct coins and `c` the amount of money to reach.
A line with `n` numbers follows with the value of each coin.
There will always be a coin with a value of 1.

# Output

For each test case a line will be printed with `n` numbers representing the amount of times the coin will be used.

# Example

Test case one has the following input:

```
5 5
1 2 3 4 5
```

The amount of `5` can be reached by only using one coin with value `5`, hence the result is

```
0 0 0 0 1
```

The second test case takes the following input:

```
5 49
1 13 16 31 44
```

To reach a value of 49 it requires the following coins.
1 \* `31`
1 \* `16`
2 \* `1`
= 49

# Explanation

The solution uses a bottom-up dynamic programming approach. Starting from a desired value of 1 every possible coin is iterated and the table updated if there is an improvement.

The following table shows a part of the solution for test case 2.
It can be read like this: To reach an amount of `15` it requires at least 3 coins, and the last coin used had index 1, which means it had a value of `13`.
Then the amount of `15 - 13 = 2` can be checked and so on to retrieve the total coins used.

| Amount of Money                                                    | 1   | 2   | 3   | 4   | 5   | ... | 15  |
| ------------------------------------------------------------------ | --- | --- | --- | --- | --- | --- | --- |
| table (min number of coins required)                               | 1   | 2   | 3   | 4   | 5   | ... | 3   |
| used_coin_table (index of last coin used to reach amount of money) | 0   | 0   | 0   | 0   | 0   | ... | 1   |
