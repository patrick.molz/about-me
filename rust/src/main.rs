use std::fs::File;
use std::io::BufReader;
use std::{io::BufRead, usize};
fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut buffer = String::new();
    let f = File::open("test.txt").expect("Unable to open file");
    let mut br = BufReader::new(f);

    // STDIN
    br.read_line(&mut buffer)?;
    let t = buffer.trim_end().parse::<usize>()?;

    // Iterate over testcases
    for k in 1..t + 1 {
        // Consume one whitespace
        br.read_line(&mut buffer)?;

        // Read l,n,d
        buffer = String::new();
        br.read_line(&mut buffer)?;
        let mut lnd = buffer.trim_end().split(" ").into_iter();
        let n = lnd.next().unwrap().parse::<i32>()?; // number of coins
        let c = lnd.next().unwrap().parse::<i32>()?; // c amount of money to be spend

        let mut coins = vec![];
        buffer = String::new();
        br.read_line(&mut buffer)?;
        let mut iter = buffer.trim_end().split(" ").into_iter();
        for _i in 0..n {
            let coin = iter.next().unwrap().parse::<i32>()?;
            coins.push(coin);
        }
        solve(c, coins, k);
    }

    return Ok(());
}

fn solve(c: i32, coins: Vec<i32>, k: usize) {
    let mut table = vec![i32::MAX; (c + 1) as usize];
    let mut used_coin_table = vec![0; (c + 1) as usize];

    table[0] = 0;
    table[1] = 1;
    used_coin_table[1] = 0;

    // i amount of money to reach
    for i in 1..table.len() {
        // Use coin[j]
        for j in 0..coins.len() {
            // Avoid negative rest
            if coins[j] <= (i as i32) {
                let current = table[i];
                let with_coin = table[i - (coins[j] as usize)] + 1;
                if current > with_coin {
                    // Use this coin
                    table[i] = with_coin;
                    used_coin_table[i] = j;
                }
            }
        }
    }
    let mut used = vec![0; coins.len()];
    let mut rest = c as usize;
    loop {
        let chosen_coin = used_coin_table[rest];
        used[chosen_coin] += 1;
        rest -= coins[chosen_coin] as usize;
        if rest == 0 {
            break;
        }
    }

    println!("Case: {}", k);
    for i in 0..coins.len() {
        print!("{} ", used[i]);
    }
    print!("\n");
}
